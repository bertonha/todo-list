# TODO List App

This is a TODO tracking app.


## Run local

### Setup environment

Global Dependencies `npm`, `bower`, and `virtualenv`.

    sudo npm install -g bower
    virtualenv .venv
    source .venv/bin/activate
    pip install -r requirements.txt

### Setup database and install bower apps

    npm install
    ./manage.py migrate

### Start development server

    ./manage.py runserver


## Run on Docker

To run project on docker you will need [docker-compose](https://docs.docker.com/compose/install/).

    docker-compose build
    docker-compose up

Open your browser on http://localhost


## Run tests

    ./manage.py tests

To run tests on Python2 and Python3.

    tox


## End to End tests

To run e2e tests you will need 2 consoles: one for the application and other for the `protractor`

* if you run the application on a docker you need to change the baseUrl

### Running protractor on local server

    npm run protractor

### Running protractor on docker server

    npm run protractor --  --baseUrl http://localhost


## Project

The Front-end is using AngularJS and the Backend is using Django.
The communication between both is done using a REST API.

REST API is defined with Django REST framework.

API urls:

```
/api/auth/login/
/api/auth/logout/
/api/lists/
/api/lists/:pk/
/api/lists/:list_pk/tasks/
/api/lists/:list_pk/tasks/:pk/
/api/user/
```

For any other url, Django will render Angular base template.

App urls:
```
/login
/logout
/signup
/todo
/todo/:pk
```

If an invalid url is send to angular then it redirects to `/todo`.
When user is not logged in, he is redirected to `/login`.

Authentication is based on Session.

Static files on local server was served by Django but on Docker ("Production") it was served by Nginx.

Project works with Python2 or Python3.

### Configurations

Keeping in mind [the twelve factor](http://12factor.net/). The settings are stored on environment variables. Local dev
as default use `DEBUG = True` and use SQLite as DB but it can be overridden. Docker use a "production" settings
`DEBUG = False`, use Postgres as DB and with DEBUG = False the JS and css are minified.
