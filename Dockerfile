FROM ubuntu

maintainer Christofer Bertonha

ENV LANG en_US.UTF-8
RUN locale-gen en_US en_US.UTF-8
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y python3.4 python3.4-dev python3-pip libpq-dev curl git \
 && curl -sL https://deb.nodesource.com/setup | bash - \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y nodejs \
 && DEBIAN_FRONTEND=noninteractive apt-get clean

RUN npm install -g bower yuglify

RUN pip3 install gunicorn

ADD requirements/* /tmp/requirements/
RUN pip3 install -r /tmp/requirements/production.txt

WORKDIR /code
