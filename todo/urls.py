from django.conf.urls import include, url
from rest_framework_extensions.routers import ExtendedDefaultRouter

from api import views
from app.views import AppView


router = ExtendedDefaultRouter()
router.register(r'user', views.UserViewSet, base_name='user')
router.register(r'lists', views.ListViewSet, base_name='list') \
      .register(r'tasks', views.TaskViewSet, base_name='task', parents_query_lookups=['list'])


urlpatterns = [
    url(r'^api/auth/', include('rest_auth.urls')),
    url(r'^api/', include(router.urls)),
    url(r'^', AppView.as_view()),
]
