from django.db import models


class List(models.Model):
    name = models.CharField(max_length=200)
    archived = models.BooleanField(default=False)
    owner = models.ForeignKey('auth.User', related_name='lists')


class Task(models.Model):
    text = models.CharField(max_length=200)
    done = models.BooleanField(default=False)
    list = models.ForeignKey(List, related_name='tasks')
