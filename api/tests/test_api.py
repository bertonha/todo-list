from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from api.models import List, Task


class ApiTaskTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user('jonson', password='123')
        self.client.login(username=self.user.username, password='123')

    def test_get_empty_lists(self):
        response = self.client.get('/api/lists/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_get_with_some_items_lists(self):
        List.objects.create(name='l1', owner=self.user)
        List.objects.create(name='l2', owner=self.user)

        expected_data = [
            {
                'id': 1,
                'name': 'l1',
                'archived': 0,
            },
            {
                'id': 2,
                'name': 'l2',
                'archived': 0,
            },
        ]

        response = self.client.get('/api/lists/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected_data)

    def test_get_only_logged_user_lists(self):
        List.objects.create(name='l1', owner=self.user)
        List.objects.create(name='l2', owner=self.user)

        user = User.objects.create_user('another_user', password='123')
        List.objects.create(name='l3', owner=user)
        List.objects.create(name='l4', owner=user)

        expected_data = [
            {
                'id': 1,
                'name': 'l1',
                'archived': 0,
            },
            {
                'id': 2,
                'name': 'l2',
                'archived': 0,
            },
        ]

        response = self.client.get('/api/lists/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected_data)

    def test_get_list_by_id(self):
        List.objects.create(name='l1', owner=self.user)

        response = self.client.get('/api/lists/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'id': 1, 'name': 'l1', 'archived': 0, 'tasks': []})

    def test_get_list_by_id_with_some_tasks(self):
        list_ = List.objects.create(name='l1', owner=self.user)
        Task.objects.create(text='t1', list=list_)
        Task.objects.create(text='t2', list=list_)
        Task.objects.create(text='t3', list=list_)

        expected_data = {
            'id': 1,
            'name': 'l1',
            'archived': 0,
            'tasks': [
                {'id': 1, 'text': 't1', 'done': 0},
                {'id': 2, 'text': 't2', 'done': 0},
                {'id': 3, 'text': 't3', 'done': 0},
            ],
        }

        response = self.client.get('/api/lists/1/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected_data)

    def test_create_a_new_list(self):
        data = {'name': 'my todo list'}
        response = self.client.post('/api/lists/', data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, {'id': 1, 'name': 'my todo list', 'archived': 0})

    def test_archive_a_list(self):
        List.objects.create(name='my todo list', owner=self.user)

        response = self.client.patch('/api/lists/1/', data={'archived': True})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, {'id': 1, 'name': 'my todo list', 'archived': 1, 'tasks': []})

    def test_add_new_task_to_a_list(self):
        List.objects.create(name='Fruits', owner=self.user)
        data = {'text': 'banana'}

        expected_data = {'id': 1, 'text': 'banana', 'done': 0}

        response = self.client.post('/api/lists/1/tasks/', data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, expected_data)

    def test_mark_task_as_done(self):
        list_ = List.objects.create(name='Fruits', owner=self.user)
        Task.objects.create(text='banana', list=list_)

        expected_task_data = {'id': 1, 'text': 'banana', 'done': 1}
        expected_data = {
            'id': 1,
            'name': 'Fruits',
            'archived': 0,
            'tasks': [
                expected_task_data,
            ],
        }

        response = self.client.patch('/api/lists/1/tasks/1/', data={'done': True})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected_task_data)

        response = self.client.get('/api/lists/1/')
        self.assertEqual(response.data, expected_data)

    def test_mark_task_as_undone(self):
        list_ = List.objects.create(name='Fruits', owner=self.user)
        Task.objects.create(text='banana', list=list_, done=True)

        expected_task_data = {'id': 1, 'text': 'banana', 'done': 0}
        expected_data = {
            'id': 1,
            'name': 'Fruits',
            'archived': 0,
            'tasks': [
                expected_task_data,
            ],
        }

        response = self.client.patch('/api/lists/1/tasks/1/', data={'done': False})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, expected_task_data)

        response = self.client.get('/api/lists/1/')
        self.assertEqual(response.data, expected_data)


class NoLoginApiTaskTestCase(APITestCase):
    def test_create_a_new_user(self):
        data = {'username': 'john', 'email': 'john.due@example.com', 'password': '123456'}
        response = self.client.post('/api/user/', data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, {'username': 'john', 'email': 'john.due@example.com'})

        self.assertTrue(User.objects.filter(username='john'))

    def test_access_denied_to_list(self):
        user = User.objects.create_user('another_jonson', password='123')
        List.objects.create(name='l1', owner=user)
        List.objects.create(name='l2', owner=user)

        response = self.client.get('/api/lists/')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {'detail': 'Authentication credentials were not provided.'})

    def test_login(self):
        User.objects.create_user('another_jonson', password='123')

        response = self.client.post('/api/auth/login/', data={'username': 'another_jonson', 'password': '123'})
        self.assertEqual(response.status_code, 200)
