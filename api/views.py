from rest_framework import mixins, permissions, viewsets
from rest_framework.viewsets import GenericViewSet
from rest_framework_extensions.mixins import DetailSerializerMixin, NestedViewSetMixin

from api.models import List, Task
from api.serializer import ListSerializer, ListDetailSerializer, TaskSerializer, UserSerializer


class UserViewSet(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = [permissions.AllowAny]
    serializer_class = UserSerializer


class ListViewSet(DetailSerializerMixin, viewsets.ModelViewSet):
    serializer_class = ListSerializer
    serializer_detail_class = ListDetailSerializer

    def get_queryset(self):
        return List.objects.filter(owner=self.request.user, archived=False)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class TaskViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        serializer.save(list_id=self.kwargs['parent_lookup_list'])
