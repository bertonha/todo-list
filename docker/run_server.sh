#!/bin/sh

bower install --allow-root
python3 manage.py collectstatic --noinput
python3 manage.py migrate
gunicorn -w 2 -b :8000 todo.wsgi:application
