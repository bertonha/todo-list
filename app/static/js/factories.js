var todoApp = angular.module('todoApp');

todoApp.factory('Auth', ['Restangular', function (Restangular) {
    return {
        login: function (credentials) {
            return Restangular.service('auth/login').post(credentials)
        },
        logout: function () {
            return Restangular.service('auth/logout').post()
        },
        user: function () {
            return Restangular.service('auth/user').get()
        }
    }
}]);

todoApp.factory('User', ['Restangular', function (Restangular) {
    return Restangular.service('user')
}]);

todoApp.factory('TodoList', ['Restangular', function (Restangular) {
    return Restangular.service('lists')
}]);
