var todoApp = angular.module('todoApp', [
    'ui.router',
    'restangular'
]);

todoApp.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', 'RestangularProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, RestangularProvider) {

        $locationProvider.html5Mode(true);

        // For any unmatched url, redirect to /login
        $urlRouterProvider.otherwise("todo");


        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'static/partials/login.html',
                controller: 'LoginCtrl'
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutCtrl'
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'static/partials/signup.html',
                controller: 'SignupCtrl'
            })
            .state('todo', {
                url: '/todo',
                templateUrl: 'static/partials/todo.html',
                controller: 'TodoListCtrl'
            })
            .state('detail', {
                url: '/todo/:id',
                templateUrl: 'static/partials/todo-detail.html',
                controller: 'TodoListDetailCtrl'
            });

        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        RestangularProvider.setBaseUrl('/api/');
        RestangularProvider.setRequestSuffix('/');
    }]);

todoApp.run(['$rootScope', '$state', 'Restangular', function ($rootScope, $state, Restangular) {

    Restangular.setErrorInterceptor(function (response) {
        if (response.status === 403 || response.status === 401) {
            $state.go('login');
            return false;
        }
    });

}]);
