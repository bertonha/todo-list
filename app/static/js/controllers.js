var todoApp = angular.module('todoApp');

todoApp.controller('LoginCtrl', ['$scope', '$state', 'Auth', function ($scope, $state, Auth) {
    $scope.errors = [];

    $scope.login = function () {
        Auth.login({username: $scope.username, password: $scope.password}).then(function () {
            $state.go('todo')
        }, function (response) {
            var errors = [];
            angular.forEach(response.data, function (error_list) {
                errors.push(error_list[0]);
            });
            $scope.errors = errors;
        })
    };
}]);

todoApp.controller('LogoutCtrl', ['$state', 'Auth', function ($state, Auth) {
    var logout = function () {
        Auth.logout();
        $state.go('login');
    };

    logout();
}]);

todoApp.controller('SignupCtrl', ['$scope', '$state', 'User', function ($scope, $state, User) {
    $scope.errors = [];

    $scope.register = function () {
        User.post({username: $scope.username, password: $scope.password, email: $scope.email}).then(function () {
            $state.go('login')
        }, function (response) {
            var errors = [];
            angular.forEach(response.data, function (error_list) {
                errors.push(error_list[0]);
            });
            $scope.errors = errors;
        })
    };
}]);

todoApp.controller('TodoListCtrl', ['$scope', 'TodoList', function ($scope, TodoList) {
    var getTodoLists = function () {
        TodoList.getList().then(function (lists) {
            $scope.todoLists = lists;
        })
    };

    $scope.addList = function () {
        TodoList.post({name: $scope.newListName}).then(function () {
            $scope.newListName = '';
            getTodoLists()
        });
    };

    getTodoLists();
}]);


todoApp.controller('TodoListDetailCtrl', ['$scope', '$state', '$stateParams', 'TodoList', 'Restangular',
    function ($scope, $state, $stateParams, TodoList, Restangular) {
        var getTodoList = function () {
            TodoList.one($stateParams.id).get().then(function (todoList) {
                Restangular.restangularizeCollection(todoList, todoList.tasks, 'tasks');
                $scope.todoList = todoList;
            }).then(function () {
            }, function () {
                $state.go('todo');
            })
        };

        $scope.addTask = function () {
            $scope.todoList.all('tasks').post({text: $scope.taskText}).then(function () {
                $scope.taskText = '';
                getTodoList();
            })
        };

        $scope.archive = function (todo) {
            todo.archived = true;
            todo.put().then(function () {
                $state.go('todo');
            });
        };

        $scope.delete = function (task) {
            task.remove().then(getTodoList)
        };

        getTodoList();
    }]);
