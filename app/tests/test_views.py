from django.test import TestCase


class ViewTestCase(TestCase):

    def test_render_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_returns_status_code_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
