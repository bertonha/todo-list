var utils = require('./utils');

describe('Signup page', function() {
  beforeEach(function () {
    utils.createAndLogin();
  });

  it('should create a new todo-list, create tasks, done some update and archive', function () {
    // create a new todolist
    element(by.model('newListName')).sendKeys('Test app');
    element(by.css('[ng-click="addList()"]')).click();
    var todoLists = element.all(by.repeater('todoList in todoLists'));
    expect(todoLists.count()).toEqual(1);

    // enter on detail
    var todoList = todoLists.get(0);
    todoList.click();

    // create some tasks
    element(by.model('taskText')).sendKeys('task 1');
    element(by.css('[ng-click="addTask()"]')).click();
    element(by.model('taskText')).sendKeys('task 2');
    element(by.css('[ng-click="addTask()"]')).click();
    element(by.model('taskText')).sendKeys('task 3');
    element(by.css('[ng-click="addTask()"]')).click();
    var tasks = element.all(by.repeater('task in todoList.tasks'));
    expect(tasks.count()).toEqual(3);

    // check 1 as done
    tasks.get(1).element(by.css('input[type=checkbox]')).click();
    var tasksDone = element.all(by.css('.done-true'));
    expect(tasksDone.count()).toEqual(1);

    // Update task_name and todolist and refresh page
    var new_todolist_name = 'Edited TODOLIST';
    var new_task_name = 'Edited Task';
    var todoListEdit = element(by.model('todoList.name'));
    var taskEdit = tasks.get(0).element(by.css('input[type=text]'));
    todoListEdit.clear().sendKeys(new_todolist_name);
    taskEdit.clear().sendKeys(new_task_name);
    // wait to puts finish
    browser.waitForAngular();
    browser.refresh();
    expect(todoListEdit.getAttribute('value')).toEqual(new_todolist_name);
    expect(taskEdit.getAttribute('value')).toEqual(new_task_name);
    expect(tasksDone.count()).toEqual(1);

    // delete one task
    tasks.get(2).element(by.css('[ng-click="delete(task)"]')).click();
    expect(tasks.count()).toEqual(2);

    // archive todolist
    element(by.css('[ng-click="archive(todoList)"]')).click();
    expect(todoLists.count()).toEqual(0);
    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/todo');
  });

});
