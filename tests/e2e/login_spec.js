var utils = require('./utils');

var username = utils.random_username();

describe('Login page', function() {
  beforeAll(function (){
    utils.create_user(username);
  });

  beforeEach(function () {
    browser.get('/logout');
    browser.get('/login');
  });

  it('should login with a valid user', function () {
    element(by.model('username')).sendKeys(username);
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/todo');
  });

  it('should alert when submit with missing username', function () {
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    var required_alert = element(by.model('username')).getAttribute('required');
    expect(required_alert.isDisplayed()).toBe(true);
  });

  it('should alert when submit with missing password', function () {
    element(by.model('username')).sendKeys('test_user');
    element(by.css('button[type="submit"]')).click();

    var required_alert = element(by.model('password')).getAttribute('required');
    expect(required_alert.isDisplayed()).toBe(true);
  });

  it('should alert when invalid username', function () {
    element(by.model('username')).sendKeys(username + 'invalid');
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    expect(element(by.css('.alert-danger')).isDisplayed()).toBe(true);
  });

  it('should alert when invalid password', function () {
    element(by.model('username')).sendKeys(username);
    element(by.model('password')).sendKeys('invalid');
    element(by.css('button[type="submit"]')).click();

    expect(element(by.css('.alert-danger')).isDisplayed()).toBe(true);
  });
});
