var random_username = function () {
  return "test" + Math.random().toString(36).substring(10);
};

var random_number = function () {
  return Math.floor((Math.random() * 100));
};

var create_user = function(username) {
  browser.get('/signup');
  element(by.model('email')).sendKeys('someemail@example.com');
  element(by.model('username')).sendKeys(username);
  element(by.model('password')).sendKeys('123456');
  element(by.css('button[type="submit"]')).click();
};

var login = function (username) {
  browser.get('/login');
  element(by.model('username')).sendKeys(username);
  element(by.model('password')).sendKeys('123456');
  element(by.css('button[type="submit"]')).click();
};

var createAndLogin = function () {
  var username = random_username();
  create_user(username);
  login(username);
};

module.exports.random_username = random_username;
module.exports.random_number = random_number;
module.exports.create_user = create_user;
module.exports.login = login;
module.exports.createAndLogin = createAndLogin;
