var utils = require('./utils');

describe('Signup page', function() {
  beforeAll(function () {
    browser.get('/logout');
  });

  beforeEach(function () {
    browser.get('/signup');
  });

  it('should create a new user', function () {
    element(by.model('email')).sendKeys('someemail@example.com');
    element(by.model('username')).sendKeys(utils.random_username());
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    expect(browser.getCurrentUrl()).toEqual(browser.baseUrl + '/login');
  });

  it('should alert when submit with missing email', function () {
    element(by.model('username')).sendKeys('test_user');
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    var required_alert = element(by.model('email')).getAttribute('required');
    expect(required_alert.isDisplayed()).toBe(true);
  });

  it('should alert when submit with missing username', function () {
    element(by.model('email')).sendKeys('someemail@example.com');
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    var required_alert = element(by.model('username')).getAttribute('required');
    expect(required_alert.isDisplayed()).toBe(true);
  });

  it('should alert when submit with missing password', function () {
    element(by.model('username')).sendKeys('test_user');
    element(by.model('email')).sendKeys('someemail@example.com');
    element(by.css('button[type="submit"]')).click();

    var required_alert = element(by.model('password')).getAttribute('required');
    expect(required_alert.isDisplayed()).toBe(true);
  });

  it('should alert when create with same username', function () {
    var username = utils.random_username();

    element(by.model('email')).sendKeys('someemail@example.com');
    element(by.model('username')).sendKeys(username);
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    browser.get('/signup');

    element(by.model('email')).sendKeys('someemail@example.com');
    element(by.model('username')).sendKeys(username);
    element(by.model('password')).sendKeys('123456');
    element(by.css('button[type="submit"]')).click();

    expect(element(by.css('.alert-danger')).isDisplayed()).toBe(true);
  });
});
